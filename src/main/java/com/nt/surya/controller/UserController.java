package com.nt.surya.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nt.surya.constants.UserRole;
import com.nt.surya.entity.User;
import com.nt.surya.service.IUserService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private IUserService service;

	@GetMapping("/register")
	public String showReg() {
		return "UserRegister";
	}

	@PostMapping("/save")
	public String saveUser(@ModelAttribute User user,
							Model model) 
	{

		Long id = service.saveUser(user);
		model.addAttribute("message", "User created with ud:" + id);
		return "UserRegister";
	}

	@GetMapping("/all")
	public String showAllUser(Model model) {
		model.addAttribute("list", service.getAllUsers());
		return "UserData";
	}

	@ResponseBody
	@GetMapping("/validateMail")
	public String validateEmail(@RequestParam("email") String email) {
		String message = "";
		if (service.findByEmail(email).isPresent()) {
			message = email + "already exist";
		}
		return message;
	}

	@GetMapping("/setup")
	public String setupUser(Authentication authentication, HttpSession session) {
		// fetch current user object
		String displayName = service.findByEmail(authentication.getName()).get().getDisplayName();
		session.setAttribute("displayName", displayName);

		String page = null;

		@SuppressWarnings("unchecked")
		String role = ((List<GrantedAuthority>) authentication.getAuthorities()).get(0).getAuthority();

		if (role.equals(UserRole.ADMIN.name())) {
			page = "/user/all";
		} else if (role.equals(UserRole.EMPLOYEE.name())) {
			page = "/brand/all";
		} else if (role.equals(UserRole.SALES.name())) {
			page = "/product/all";
		} else if (role.equals(UserRole.CUSTOMER.name())) {
			page = "/product/search";
		}
		return "redirect:" + page;
	}

	@GetMapping("login")
	public String showLoginPage() {
		return "UserLogin";
	}
}
