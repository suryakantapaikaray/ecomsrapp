package com.nt.surya.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nt.surya.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
