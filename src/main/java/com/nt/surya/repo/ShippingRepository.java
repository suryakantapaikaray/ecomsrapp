package com.nt.surya.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nt.surya.entity.Shipping;

public interface ShippingRepository extends JpaRepository<Shipping, Long> {

}
