package com.nt.surya.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nt.surya.entity.Coupon;

public interface CouponRepository extends JpaRepository<Coupon, Long> {

}
