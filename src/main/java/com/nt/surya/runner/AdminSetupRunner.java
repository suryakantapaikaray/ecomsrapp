package com.nt.surya.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.nt.surya.constants.UserRole;
import com.nt.surya.entity.User;
import com.nt.surya.repo.UserRepository;

@Component
public class AdminSetupRunner implements CommandLineRunner {

	@Value("${app.admin.email}")
	private String username;

	@Autowired
	private UserRepository repository;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Override
	public void run(String... args) throws Exception {

		if (!repository.findByEmail(username).isPresent()) {
			User user = new User();
			user.setDisplayName("ADMIN");
			user.setEmail(username);
			user.setPassword(encoder.encode("admin"));
			user.setRole(UserRole.ADMIN);
			user.setStatus("ACTIVE");
			user.setAddress("NONE");
			user.setContact("8093445891");

			repository.save(user);
		}
	}

}
