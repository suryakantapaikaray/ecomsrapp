package com.nt.surya.service;

import java.util.List;
import java.util.Map;

import com.nt.surya.entity.Brand;

public interface IBrandService {

	Long saveBrand(Brand b);

	void updateBrand(Brand b);

	void deleteBrand(Long b);

	Brand getOneBrand(Long id);

	List<Brand> getAllBrands();

	Map<Long, String> getBrandIdAndName();
}
