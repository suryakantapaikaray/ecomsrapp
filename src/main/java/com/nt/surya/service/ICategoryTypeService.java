package com.nt.surya.service;

import java.util.List;
import java.util.Map;

import com.nt.surya.entity.CategoryType;

public interface ICategoryTypeService {

	Long saveCategoryType(CategoryType categoryType);

	void updateCategoryType(CategoryType categoryType);

	void deleteCategoryType(Long id);

	CategoryType getOneCategoryType(Long id);

	List<CategoryType> getAllCategoryTypes();
	
	Map<Integer,String> getCategoryTypeIdAndName();

}
