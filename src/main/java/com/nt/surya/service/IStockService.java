package com.nt.surya.service;

import java.util.List;

import com.nt.surya.entity.Stock;

public interface IStockService {

	Long createStock(Stock stock);

	void updateStock(Long id, long count);

	Long getStockIdByProduct(Long productId);

	List<Stock> getStockDetails();
}
