package com.nt.surya.service;

import java.util.List;

import com.nt.surya.entity.Customer;

public interface ICustomerService {

	Long saveCustomer(Customer user);
	
	List<Customer> getAllCustomers();
}
