package com.nt.surya.service;

import java.util.List;
import java.util.Optional;

import com.nt.surya.entity.User;

public interface IUserService {

	Long saveUser(User user);
	
	Optional<User> findByEmail(String email);
	
	List<User> getAllUsers();

//	Long saveUser(User user);
}
