package com.nt.surya.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nt.surya.entity.Shipping;
import com.nt.surya.exception.ShippingNotFoundException;
import com.nt.surya.repo.ShippingRepository;
import com.nt.surya.service.IShippingService;

@Service
public class ShippingServiceImpl implements IShippingService {

	@Autowired
	private ShippingRepository repo;
	
	@Override
	public Long saveShipping(Shipping shipping) {
		return repo.save(shipping).getId();
	}

	@Override
	public void updateShipping(Shipping shipping) {
		if(shipping.getId()==null || !repo.existsById(shipping.getId()))
			throw new  ShippingNotFoundException("Shipping Not exist");
		else
			repo.save(shipping);
	}

	@Override
	public void deleteShipping(Long id) {
		repo.delete(getOneShipping(id));
	}

	@Override
	public Shipping getOneShipping(Long id) {
		return repo.findById(id).orElseThrow(
				()->new ShippingNotFoundException("Shipping Not exist")
				);
	}

	@Override
	public List<Shipping> getAllShipping() {
		return repo.findAll();
	}

}
