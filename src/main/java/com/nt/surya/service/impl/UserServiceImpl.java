package com.nt.surya.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.nt.surya.constants.UserStatus;
import com.nt.surya.entity.User;
import com.nt.surya.repo.UserRepository;
import com.nt.surya.service.IUserService;
import com.nt.surya.util.AppUtil;

@Service
public class UserServiceImpl implements IUserService, UserDetailsService{ //TODO

	@Autowired
	private UserRepository repo;
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Override
	public Long saveUser(User user) {
		//Generating password
		String pwd=AppUtil.genPwd();
		System.out.println(user.getEmail()+"-"+pwd+"-"+user.getRole().name());
		
		//read generated pwd and encode
		String encPwd=encoder.encode(pwd);
		//set back to user object
		user.setPassword(encPwd);
		
		
		user.setStatus(UserStatus.INACTIVE.name());
		//TODO: SENDIN EMAIL
		return repo.save(user).getId();
	}

	@Override
	public Optional<User> findByEmail(String email) {
		return repo.findByEmail(email);
	}

	@Override
	public List<User> getAllUsers() {
		return repo.findAll();
	}

	
	public UserDetails loadUserByUsername(String username)
											throws UsernameNotFoundException{
	
		//Load user by username(email)
		Optional<User> opt=findByEmail(username);
		if(!opt.isPresent()) {
			throw new UsernameNotFoundException("Not exist");
		}
			else {
				//read user object
				User user=opt.get();
				return new org.springframework.security.core.userdetails
						//username,password, List<GA>(RoleAsString)
						.User(
								user.getEmail(),
								user.getPassword(),
								Arrays.asList(
										
										new SimpleGrantedAuthority(
												user.getRole().name()
												)
										)
								);
			}
		}
}


