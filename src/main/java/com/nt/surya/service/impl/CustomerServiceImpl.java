package com.nt.surya.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nt.surya.entity.Customer;
import com.nt.surya.repo.CustomerRepository;
import com.nt.surya.service.ICustomerService;

@Service
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	private CustomerRepository repo;

	@Override
	public Long saveCustomer(Customer user) {
		return repo.save(user).getId();
	}

	@Override
	public List<Customer> getAllCustomers() {
		return repo.findAll();
	}

}
