package com.nt.surya.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nt.surya.entity.CategoryType;
import com.nt.surya.repo.CategoryTypeRepository;
import com.nt.surya.service.ICategoryTypeService;
import com.nt.surya.util.AppUtil;

@Service
public class CategoryTypeServiceImpl implements ICategoryTypeService {

	@Autowired
	private CategoryTypeRepository repo;

	@Override
	@Transactional
	public Long saveCategoryType(CategoryType categoryType) {
		return repo.save(categoryType).getId();
	}

	@Override
	@Transactional
	public void updateCategoryType(CategoryType categoryType) {
		repo.save(categoryType);

	}

	@Override
	@Transactional
	public void deleteCategoryType(Long id) {
		repo.deleteById(id);

	}

	@Override
	@Transactional(readOnly = true)
	public CategoryType getOneCategoryType(Long id) {
		return repo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<CategoryType> getAllCategoryTypes() {
		return repo.findAll();
	}

	@Override
	public Map<Integer, String> getCategoryTypeIdAndName() {
		List<Object[]> list = repo.getCategoryTypeIdAndName();
		return AppUtil.covertListToMap(list);
	}

}
