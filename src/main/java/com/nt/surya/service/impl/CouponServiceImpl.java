package com.nt.surya.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nt.surya.entity.Coupon;
import com.nt.surya.repo.CouponRepository;
import com.nt.surya.service.ICouponService;

@Service
public class CouponServiceImpl implements ICouponService {

	@Autowired
	private CouponRepository repo;

	@Override
	@Transactional
	public Long saveCoupon(Coupon coupon) {
		return repo.save(coupon).getId();
	}

	@Override
	@Transactional
	public void updateCoupon(Coupon coupon) {
		repo.save(coupon);

	}

	@Override
	@Transactional
	public void deleteCoupon(Long id) {
		repo.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Coupon getOneCoupon(Long id) {
		return repo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Coupon> getAllCoupons() {
		return repo.findAll();
	}

}
