package com.nt.surya.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nt.surya.entity.Stock;
import com.nt.surya.repo.StockRepository;
import com.nt.surya.service.IStockService;

@Service
public class StockServiceImpl implements IStockService {

	@Autowired
	private StockRepository repo;

	@Override
	public Long createStock(Stock stock) {
		return repo.save(stock).getId();
	}

	@Override
	@Transactional
	public void updateStock(Long id, long count) {
		repo.updateStock(id, count);

	}

	@Override
	public List<Stock> getStockDetails() {
		return repo.findAll();
	}

	@Override
	public Long getStockIdByProduct(Long productId) {
		return repo.getStockIdByProduct(productId);
	}
}
