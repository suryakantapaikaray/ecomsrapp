package com.nt.surya.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nt.surya.entity.Product;
import com.nt.surya.exception.ProductNotFoundException;
import com.nt.surya.repo.ProductRepository;
import com.nt.surya.service.IProductService;
import com.nt.surya.util.AppUtil;

@Service
public class ProductServiceImpl implements IProductService {

	
	@Autowired
	private ProductRepository repo;
	
	@Override
	public Long saveProduct(Product p) {
		return repo.save(p).getId();
	}

	@Override
	public void updateProduct(Product p) {
		if(p.getId()==null || repo.existsById(p.getId()))
			throw new ProductNotFoundException("Product Not Found");
		else
			repo.save(p);
	}

	@Override
	public void deleteProduct(Long id) {
		repo.delete(getOneProduct(id));

	}

	@Override
	public Product getOneProduct(Long id) {
		return repo.findById(id)
				.orElseThrow(()->new ProductNotFoundException("Product Not Found"));
	}

	@Override
	public List<Product> getAllProducts() {	
		return repo.findAll();
	}

	@Override
	public Map<Long, String> getProductIdAndName() {
		List<Object[]> list=repo.getProductIdAndNames();
		return AppUtil.convertListToMapLong(list);
	}

}
