package com.nt.surya.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nt.surya.entity.Brand;
import com.nt.surya.repo.BrandRepository;
import com.nt.surya.service.IBrandService;
import com.nt.surya.util.AppUtil;

@Service
public class BrandServiceImpl implements IBrandService {

	
	@Autowired
	private BrandRepository repo;
	
	@Override
	public Long saveBrand(Brand b) {
		return repo.save(b).getId();
	}
	

	@Override
	public void updateBrand(Brand b) {
			repo.save(b);

	}

	@Override
	public void deleteBrand(Long b) {
		repo.deleteById(b);

	}

	@Override
	public Brand getOneBrand(Long id) {
		Optional<Brand> opt=repo.findById(id);
		if(opt.isPresent())
			return opt.get();
		return null;
	}

	@Override
	public List<Brand> getAllBrands() {
		return repo.findAll();
	}

	@Override
	public Map<Long, String> getBrandIdAndName() {
		List<Object[]> list=repo.getBrandIdAndName();
		return AppUtil.convertListToMapLong(list);
	}

}
